﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using web_app.Models;

namespace web_app.Controllers
{
    public class HelloController : Controller
    {
        private readonly ILogger<HelloController> _logger;

        public HelloController(ILogger<HelloController> logger)
        {
            _logger = logger;
        }

        //public String Index()
        public IActionResult Index()
        {
            //return "test";
            
            //String conn = @"Server=localhost;Port=5432;UserId=admin;Password=admin;Database=aaa";
            //using (NpgsqlConnection connection = new NpgsqlConnection(conn))
            //{
                // bookというテーブルのレコード数を取得
                //var count = connection.Query<int>("SELECT COUNT(*) FROM book").First();
                //Console.WriteLine(count);
            //}
            
//            using NpgsqlConnection con =  new("Server=127.0.0.1; Port=5432; User Id=admin; Password=admin; Database=pg; SearchPath=public");
//			con.Open();

            return View();
        }

        //ublic String aaa()
        public IActionResult aaa()
        {
            //return "aaa";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
